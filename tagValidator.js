class ValidateTags {
  constructor(html) {
    this._html = html
    this._tags = this.getTags(html)
    this._openedTags = []
  }

  getTags(html) {
    return html.match(/(<{1}(\/?)[A-Z]+>{1})/g)
  }

  stripSpecialChars(html) {
    return html && html.replace(/[\<\>\/]*/g, '')
  }

  getInfo(current, lastItem) {
    let expected = this._openedTags.pop() || '#'
    let found = current || '#'
    let isTagDiff = this.stripSpecialChars(found) != this.stripSpecialChars(expected)
    let isTagEqual = !isTagDiff
    return { expected, found, isTagDiff, isTagEqual }
  }

  getResponse() {
    let info
    for (let index in this._tags) {
      let current = this._tags[index]
      let lastItem = (parseInt(index) + 1) == this._tags.length

      if (!/[\/]/g.test(current)) {
        this._openedTags.push(current)
        continue
      }

      info = this.getInfo(current, lastItem)

      if (lastItem && this._openedTags.length > 0) {
        info = this.getInfo(current, lastItem)
        info.found = '#'
      }

      if (info.isTagDiff) {
        return `Expected ${info.expected.replace('<', '<\/')} found ${info.found}`
      }

      if (info.isTagEqual && lastItem) {
        return 'Correctly tagged paragraph'
      }
    }
  }
}

ValidateTags.check = (html) => {
  return new ValidateTags(html).getResponse()
}

module.exports = ValidateTags
