const assert = require('assert');
const ValidateTags = require('./tagValidator')

describe('Tag Checker Problem', function() {
  it('test 1', function() {
    const res = ValidateTags.check('The following text<C><B>is centred and in boldface</B></C>')
    assert.equal(res, 'Correctly tagged paragraph');
  });

  it('test 2', function() {
    const res = ValidateTags.check('<B>This <\\g>is <B>boldface</B> in <<*> a</B> <\\6> <<d>sentence')
    assert.equal(res, 'Correctly tagged paragraph');
  });

  it('test 3', function() {
    const res = ValidateTags.check('<B><C> This should be centred and in boldface, but the tags are wrongly nested </B></C>')
    assert.equal(res, 'Expected <\/C> found <\/B>');
  });

  it('test 4', function() {
    const res = ValidateTags.check('<B>This should be in boldface, but there is an extra closing tag</B></C>')
    assert.equal(res, 'Expected # found </C>');
  });

  it('test 5', function() {
    const res = ValidateTags.check('<B><C>This should be centred and in boldface, but there is a missing closing tag</C>')
    assert.equal(res, 'Expected </B> found #');
  });
});
